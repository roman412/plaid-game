#include <time.h>
#include <stdlib.h>
#include  <GL/glut.h>

int N = 30, M = 20; //���������� ������ �� ����������� � ���������
int Scale = 20; //������� �����

int w = Scale * N; //������ �������� ����
int h = Scale * M; //������ �������� ����

int dir = 0; //����������� ��������

struct
{
	int x; int y;
}  s; //������� ���������

void reshape(int w, int h) { //���������� ��������� �������� ����; ����������� ����
	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Tick() //���� ���� ����������� �����
{
	//�������� �� ����������� dir
	s.y = (dir == 0) ? (s.y + 1) : s.y;
	s.x = (dir == 1) ? (s.x - 1) : s.x;
	s.x = (dir == 2) ? (s.x + 1) : s.x;
	s.y = (dir == 3) ? (s.y - 1) : s.y;
	//��������� �������
	dir = (s.x > N - 3) ? 1 : dir;
	dir = (s.x < 2) ? 2 : dir;
	dir = (s.y > M - 3) ? 3 : dir;
	dir = (s.y < 2) ? 0 : dir;
}

void DrawField() //��������� ������� ����
{
	//��������� �����
	glColor3f(0.3, 0.1, 1.0);
	glBegin(GL_LINES);
	for (int i = 0; i<w; i += Scale) { glVertex2f(i, 0); glVertex2f(i, h); }
	for (int j = 0; j<h; j += Scale) { glVertex2f(0, j); glVertex2f(w, j); }
	glEnd();
	//��������� �������
	glColor3f(0.7, 0.3, 0.5);
	for (int i = 0; i < w; i += Scale) { glRectf(i, h, i + Scale, h - Scale); glRectf(i, 0, i + Scale, Scale); }
	for (int j = 0; j < h; j += Scale) { glRectf(w, j, w - Scale, j + Scale); glRectf(0, j, Scale, j + Scale); }
}

void DrawHero() //���������� �����
{
	glColor3f(0.0, 0.0, 1.0);
	glRectf(s.x*Scale, s.y*Scale, (s.x + 0.99)*Scale, (s.y + 0.99)*Scale);
}

void display() { //��������� ����������� ���� ����� (���������� �� 'timer(...)' )
	glClear(GL_COLOR_BUFFER_BIT);

	DrawField();
	DrawHero();

	glutSwapBuffers(); //������� ������������
}

void KeyboardEvent(int key, int a, int b) //����������� ������� ������, � ����� ����������� � ������������
{
	switch (key)
	{
	case 101:  dir = 0; break;
	case 102:  dir = 2; break;
	case 100:  dir = 1; break;
	case 103:  dir = 3; break;
	}
}

void timer(int = 0) //������ ������� ��������� ����
{
	display();

	Tick(); //���� '���' (����) �������

	glutTimerFunc(120, timer, 0); //������ ��������� �� 1 ��������� � 120��
}

int main(int argc, char * argv[]) //���� ���������
{
	s.x = 10; //��������� ���������� �����
	s.y = 10;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA); //������������ ���������� ������: � ����� ������ - ������� ������������ + RGBA ����� (RGBA - RedGreenBlueAlpha)
	glutInitWindowSize(w, h); //������� ���� (���������� � ����������� �� ���������� ������ � �� ��������)
	glutInitWindowPosition(400, 200); //������� �������� ���� (����������: ���������� ����������, � �� �������������)
	glutCreateWindow("Game"); //�������� ����
	glClearColor(0.4, 0.4, 0.6, 1.0); //���� ����
	glMatrixMode(GL_PROJECTION); //������������ ������
	glLoadIdentity();
	gluOrtho2D(0, w, 0, h); //��������� ��������� (��� 2D �� �������������)

	glutReshapeFunc(reshape); //����� ��������� ����
	glutDisplayFunc(display); //����� ��������� �����
	glutSpecialFunc(KeyboardEvent); //����� ������������ ������������ �������

	glutTimerFunc(50, timer, 0);

	glutMainLoop();

}